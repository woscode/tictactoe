package com.teamzero.tictactoe;

import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class HttpRequestTest {

	@LocalServerPort
	private int port;

	@Autowired
	private TestRestTemplate restTemplate;
	
	private final String homePage = "http://localhost:" + port + "/tictactoe/";

	@Test
	void mainPageShouldReturnDefaultMessage() throws Exception {
		
		assertThat(this.restTemplate.getForObject(homePage, String.class)).isNull();
		
		String requestParam = "playerID=123";
		assertThat(this.restTemplate.getForObject(homePage + "?" + requestParam, String.class)).contains("123");
	}
}