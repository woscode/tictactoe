package com.teamzero.tictactoe.service;

import java.util.List;

import com.teamzero.tictactoe.dto.GameDTO;
import com.teamzero.tictactoe.dto.PlayerDTO;
import com.teamzero.tictactoe.model.Game;
import com.teamzero.tictactoe.model.GameState;

public interface GameService {

	Game getGameById(String gameId);
	Game getGameByPlayer(String gameId, PlayerDTO playerDTO);
	
	GameDTO getById(String gameId);
	GameDTO getByPlayer(String gameId, PlayerDTO playerDTO);
	GameDTO createByPlayer(PlayerDTO playerDTO);
	GameDTO move(String gameId, PlayerDTO playerDTO, Integer position);
	GameDTO joinPlayer(String gameId, PlayerDTO playerDTO);
	
	List<GameDTO> getAll();
	List<GameDTO> getAllAvailable();
	GameDTO getAvailableByGameState(GameState gameState);
	
	GameDTO getAvailableByPlayer(PlayerDTO playerDTO);
}
