package com.teamzero.tictactoe.service;

import java.util.List;

import com.teamzero.tictactoe.dto.PlayerDTO;
import com.teamzero.tictactoe.dto.PlayerStatsDTO;
import com.teamzero.tictactoe.model.Player;


public interface PlayerService {
	
	Player getPlayerById(String playerId);
	
	PlayerDTO getById(String playerId);
    PlayerDTO newPlayer(String name);
    
    List<PlayerStatsDTO> getStatistics();
}
