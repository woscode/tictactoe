package com.teamzero.tictactoe.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.teamzero.tictactoe.model.Player;

@Repository
public interface PlayerRepository extends CrudRepository<Player, String> {

}