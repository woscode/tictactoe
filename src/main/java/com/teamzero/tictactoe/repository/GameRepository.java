package com.teamzero.tictactoe.repository;


import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.teamzero.tictactoe.model.Game;
import com.teamzero.tictactoe.model.GameState;

@Repository
public interface GameRepository extends CrudRepository<Game, String> {

	Game findByGameState(GameState gameState);
	List<Game> findAllByGameState(GameState gameState);
	
}