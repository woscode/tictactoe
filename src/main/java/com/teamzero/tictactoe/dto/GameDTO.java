package com.teamzero.tictactoe.dto;

import javax.validation.constraints.NotEmpty;

import com.teamzero.tictactoe.model.Board;
import com.teamzero.tictactoe.model.Game;
import com.teamzero.tictactoe.model.GameState;
import com.teamzero.tictactoe.model.Player;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GameDTO {
	
	@NotEmpty
    private String id;
    
    private GameState gameState;
    private Board board;
    
    private Player playerX;
    private Player playerO;
    
    public GameDTO() { }
    
    public GameDTO(Game gameEntity) {
    	
    	this.id = gameEntity.getId();
    	this.gameState = gameEntity.getGameState();
    	this.board = gameEntity.getBoard();
    	this.playerX = gameEntity.getPlayerX();
    	this.playerO = gameEntity.getPlayerO();
    	
    }
}
